import cv2
import numpy as np
from unittest.mock import MagicMock, patch
from camera_mod import CameraModule

def dynamically_add_method(cls, method_name):

    method = MagicMock()
    setattr(cls, method_name, method)

@patch('cv2.VideoCapture')
def test_detect_motion(mock_cv2_video_capture):

    video_capture_instance = mock_cv2_video_capture.return_value
    frame_with_contour = np.zeros((480, 640, 3), dtype=np.uint8)
    cv2.rectangle(frame_with_contour, (100, 100), (200, 200), (255, 255, 255), thickness=-1)
    video_capture_instance.read.return_value = (True, frame_with_contour)


    dynamically_add_method(CameraModule, 'capture_frames')
    dynamically_add_method(CameraModule, 'background_capture')
    dynamically_add_method(CameraModule, 'background_capture_snap')
    dynamically_add_method(CameraModule, 'detect_motion')

    camera_module = CameraModule()


    camera_module.detect_motion.return_value = False


    motion_detected = camera_module.detect_motion(None)
    assert not motion_detected


    camera_module.detect_motion.return_value = True
    motion_detected = camera_module.detect_motion(video_capture_instance.read.return_value[1])
    assert motion_detected


if __name__ == "__main__":
    pass
