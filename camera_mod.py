import cv2
import glob
import os
import threading
import datetime
import time
import multiprocessing
import asyncio
from aiogram import Bot, Dispatcher, types
from aiogram.utils import executor
import telegram



class CameraModule:



    def __init__(self):

        self.camera = cv2.VideoCapture(0)           
        self.frame_queue = multiprocessing.Queue()
        self.capture_process = multiprocessing.Process(target=self.capture_frames)
        self.capture_process.start()
        self.motion_detected = False
        self.person_cascade = cv2.CascadeClassifier('haarcascade_fullbody.xml')
        self.face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        self.motion_count = 0
        self.recording = False
        self.out = None
        self.background = None
        self.last_motion_time = 0
        self.manager = multiprocessing.Manager()
        self.camera_lock = self.manager.Lock()
        self.RECORDING_DURATION = 20  # 20 seconds
        self.MAX_MOTION_COUNT = 15 # 15 motions x 20seconds for start rec
        self.FRAME_RATE = 15 # FPS Frame per Second, DON'T TOUCH THIS PARAMETER
        self.RESOLUTION = (640, 480) # Video Resolution 640x480
        self.recording_lock = threading.Lock()
        self.MESSAGE_BOT = 'Rilevato movimento/volto: '

        _, self.background = self.camera.read()


        # Avvia il thread per il rilevamento e la registrazione in background

        self.camera_thread = threading.Thread(target=self.background_capture, args=())
        self.camera_thread.daemon = True
        self.camera_thread.start()

        self.snapshot_thread = threading.Thread(target=self.background_capture_snap)
        self.snapshot_thread.daemon = True
        self.snapshot_thread.start()



        #Verifica nel costruttore se la camera è attiva
        try:
            if self.camera is not False:
                print(f"Rilevamento Motion attivato:  Start Service {datetime.datetime.now().strftime('%d/%m/%Y %H:%M:%S')}")
        except Exception as e:
            print(f'Qualcosa è andato storto...{e}')
            exit()



    def camera_loop(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_until_complete(self.run_camera())

    async def run_camera(self):
        await self.background_capture(camera)
        

    def detect_motion(self, frame=None):
        if frame is None or frame.size == 0:
            return False
        else:
            return True

        self.gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        self.gray_frame = cv2.GaussianBlur(self.gray_frame, (21, 21), 0)

        if self.background is None:
            self.background = self.gray_frame
            return False

        if self.background.shape != self.gray_frame.shape:
            self.background = self.gray_frame
            return False

        frame_delta = cv2.absdiff(self.background, self.gray_frame)
        thresh = cv2.threshold(frame_delta, 25, 255, cv2.THRESH_BINARY)[1]
        thresh = cv2.dilate(thresh, None, iterations=2)
        contours, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        motion_detected = False
        for contour in contours:
            if cv2.contourArea(contour) < 500:
                continue
            motion_detected = True
            break


        if motion_detected and self.detect_people(frame):
            self.background = self.gray_frame
        return motion_detected


    def start_recording(self):
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
        current_video_filename = f'videos/video_{timestamp}.mp4'
        
        try:
            self.out = cv2.VideoWriter(current_video_filename, fourcc, self.FRAME_RATE, self.RESOLUTION)
            self.recording = True
            self.last_motion_time = time.time()
        except Exception as e:
            print(f"Error starting recording: {e}")


    def stop_recording(self):
        if self.out is not None:
            self.out.release()
            self.out = None
        self.recording = False

    
    def detect_people(self, frame):
        if self.camera is None:
            return frame, False  # Restituisci il frame originale e informazione sulla mancanza di persone
        if frame is None:
            return None, False

        gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        persons = self.person_cascade.detectMultiScale(gray_frame, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))
        faces = self.face_cascade.detectMultiScale(gray_frame, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))

        frame_with_rect = frame.copy()

        for (x, y, w, h) in persons:
            cv2.rectangle(frame_with_rect, (x, y), (x + w, y + h), (0, 255, 20), 2)

        for (x, y, w, h) in faces:
            cv2.rectangle(frame_with_rect, (x, y), (x + w, y + h), (0, 0, 255), 2)

        persons_list = list(persons) if len(persons) > 0 else []
        faces_list = list(faces) if len(faces) > 0 else []

        if persons_list or faces_list:
            print(f"Persone/volti rilevati: {len(persons_list) + len(faces_list)}")
            for (x, y, w, h) in persons_list:
                print(f"Coordinate persona: x={x}, y={y}, width={w}, height={h}")
            for (x, y, w, h) in faces_list:
                print(f"Coordinate volto: x={x}, y={y}, width={w}, height={h}")

        return frame_with_rect, (len(persons_list) > 0 or len(faces_list) > 0), persons_list + faces_list
        #return frame_with_rect, (len(faces_list) > 0) 





    def generate_frames(self):
        while True:
            with self.camera_lock:
                if self.camera.isOpened():
                    success, frame = self.camera.read()
                    if success:
                        timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
                        cv2.putText(frame, timestamp, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 20), 2)

                        motion_detected = self.detect_motion(frame)
                        frame_with_rect, _, _ = self.detect_people(frame)

                        if motion_detected and not self.recording:
                            if motion_detected:
                                self.motion_count += 1
                                if self.motion_count >= self.MAX_MOTION_COUNT:
                                    self.last_motion_time = time.time()
                            else:
                                self.motion_count = 0

                        if self.recording and time.time() - self.last_motion_time > self.RECORDING_DURATION:
                            self.stop_recording()

                        if self.recording:
                            self.out.write(frame)

                        yield frame_with_rect



    def background_capture(camera):
        
        asyncio.set_event_loop(asyncio.new_event_loop())
        loop = asyncio.get_event_loop()
        while True:
            if camera.camera is None:
                camera.initialize_camera()

            success, frame = camera.camera.read()
            if not success:
                break
            else:
                timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
                cv2.putText(frame, timestamp, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 20), 2)
                motion_detected = camera.detect_motion(frame)
                frame_with_rect, person_detected, persons = camera.detect_people(frame)

                with camera.recording_lock:
                    if motion_detected and person_detected and not camera.recording:
                        camera.motion_count += 1
                        if camera.motion_count >= camera.MAX_MOTION_COUNT:
                            camera.last_motion_time = time.time()

                        if camera.last_motion_time + camera.RECORDING_DURATION >= time.time():
                            camera.start_recording()
                            camera.capture_frame()
                            photo_path = "frames/"
                            frame_files = sorted(glob.glob(photo_path + '*.jpg'))
                            if frame_files:
                                latest_frame_filename = frame_files[-1]
                                loop.run_until_complete(camera.invia_frame(latest_frame_filename))

                    if camera.recording and time.time() - camera.last_motion_time > camera.RECORDING_DURATION:
                        camera.stop_recording()

                    if camera.recording:
                        camera.out.write(frame)
                time.sleep(0.01)


    async def invia_frame(self, photo):
        bot_token = "BOT_TOKEN_TELEGRAM" 
        bot = telegram.Bot(token=bot_token)
        try:
            with open(photo, "rb") as photo_file:
                await bot.send_photo(chat_id="NUMBER_ID_USER_TELEGRAM", photo=photo_file, caption=self.MESSAGE_BOT + datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S'))
                if photo_file.closed:
                    pass
                else:
                    photo_file.close
                time.sleep(0.01)
        except Exception as e:
            print(f'Raise error: {e}')

            
    """USE IT IF YOU DON’T KNOW YOUR TELEGRAM NUMBER_USER_ID
       IN DEBUGGING YOU CAN SEE YOUR NUMBER_USER_ID
    async def msg_command(self, message: types.Message):
        mio = message.message_id
        if message.text == '/photo':
            if self.snapshot_filename:
                with open(motion_or_capture, "rb") as photo_file:
                    await message.answer_photo(photo=photo_file)
            else:
                await message.answer("Nessuna foto disponibile al momento.")
    """

    def capture_frames(self):
        while True:
            if self.camera is None:
                self.initialize_camera()

            success, frame = self.camera.read()
            if not success:
                break
            else:
                timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
                cv2.putText(frame, timestamp, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 20), 2)
                motion_detected = self.detect_motion(frame)
                frame_with_rect, person_detected, persons = self.detect_people(frame)

                if motion_detected and person_detected and not self.recording:
                    self.motion_count += 1
                    if self.motion_count >= self.MAX_MOTION_COUNT:
                        self.last_motion_time = time.time()

                    if self.last_motion_time + self.RECORDING_DURATION >= time.time():
                        self.start_recording()
                        self.capture_frame()
 

                if self.recording and time.time() - self.last_motion_time > self.RECORDING_DURATION:
                    self.stop_recording()

                if self.recording:
                    self.out.write(frame)

                _, buffer = cv2.imencode('.jpg', frame)
                frame_with_timestamp = buffer.tobytes()
                yield (b'--frame\r\n'
                       b'Content-Type: image/jpeg\r\n\r\n' + frame_with_timestamp + b'\r\n')



    def capture_frame(self):
        if self.camera is not None:
            success, frame = self.camera.read()
            if success:
                timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
                filename = f'frames/frame_{timestamp}.jpg'
                cv2.imwrite(filename, frame)
                return f"Frame scattato {filename}"  # Restituisce l'URL dell'immagine
            else:
                return "Errore nella cattura del frame"
        else:
            return "Camera non inizializzata"


    def background_capture_snap(self):
        snapshot_interval = 60
        last_snapshot_time = time.time() - snapshot_interval
        while True:
            if self.camera is None:
                self.initialize_camera()

            current_time = time.time()
            if current_time - last_snapshot_time >= snapshot_interval:
                success, frame = self.camera.read()
                if success:
                    timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
                    filename = f'snapshots/snapshot_{timestamp}.jpg'
                    cv2.imwrite(filename, frame)
                    last_snapshot_time = current_time
            time.sleep(1)



if __name__ == "__main__":
    bot = Bot(token="BOT_TOKEN_TELEGRAM")
    camera = CameraModule()
    dp = Dispatcher(bot)
    #dp.register_message_handler(camera.msg_command, commands=['photo'])
    capture_thread = threading.Thread(target=camera.background_capture, args=())
    capture_thread.start()
    executor.start_polling(dp, skip_updates=True)
