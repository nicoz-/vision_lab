import json
from flask import Flask, Response, render_template, request, redirect, url_for, jsonify
from flask_basicauth import BasicAuth
from flask import send_from_directory
import ssl
import os
import cv2
import datetime
import camera_mod


os.environ['REQUESTS_CA_BUNDLE'] = ""
app = Flask(__name__)
app.config['BASIC_AUTH_USERNAME'] = '' # INSERT YOUR USERNAME
app.config['BASIC_AUTH_PASSWORD'] = '' # INSERT YOUR PASSWORD
app.config['UPLOAD_FOLDER'] = 'videos/'
app.secret_key = '' # INSERT YOUR SECRET KEY 
#certificate = '' # ADDING YOUR CERTIFICATE
#key_cert = '' # ADDING YOUR KEY
basic_auth = BasicAuth(app)


camera = camera_mod.CameraModule()
#context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
'''try:
    context.load_cert_chain(certificate, key_cert)
except ssl.SSLError as e:
    print("Errore nel caricamento del certificato o della chiave:", e)
    breakpoint'''


@app.route('/')
@basic_auth.required
def index():
    return render_template('index.html')


@app.route('/video_feed')
@basic_auth.required
def video_feed():
    return Response(generate_frames(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


def generate_frames():
    for frame_with_rect in camera.generate_frames():
        _, buffer = cv2.imencode('.jpg', frame_with_rect)
        frame_with_timestamp = buffer.tobytes()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame_with_timestamp + b'\r\n')




@app.route('/start_motion_detection')
def start_motion_detection():
    camera.motion_detected = True
    if camera.motion_detected is True:
        return "Motion detection started"
    else:
        return "Non è possibile avviare le motions, riavvia il demone"


@app.route('/stop_motion_detection')
def stop_motion_detection(): 
    camera.motion_detected = False
    if camera.motion_detected is False:
        return "Motion detection stopped"
    else:
        return "Non è possibile stoppare le motions, prova a killare il demone"


@app.route('/capture_frame', methods=['POST'])
@basic_auth.required
def capture_frame_route():
    image_url = camera_mod.capture_frame()
    return json.dumps({'image_url': image_url})


@app.route('/file_viewer')
@basic_auth.required
def file_viewer():
    folder_path = 'videos/'
    video_files = [file for file in os.listdir(folder_path) if file.endswith('.mp4')]
    return render_template('file_viewer.html', video_files=video_files)


@app.route('/video/<filename>')
@basic_auth.required
def video(filename):
    video_path = os.path.join('videos', filename)
    return Response(open(video_path, 'rb'), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/download/<filename>')
@basic_auth.required
def download_video(filename):
    video_path = os.path.join('videos', filename)
    return send_from_directory(video_path, as_attachment=True)


@app.route('/frames')
@basic_auth.required
def list_images():
    image_folder = '/home/nicoz/Desktop/flask_streamcam/flask_stream_stable/frames'
    image_names = [file for file in os.listdir(image_folder) if file.endswith('.jpg')]
    image_names.sort(reverse=True)
    return render_template('frames_view.html', image_names=image_names)


@app.route('/frames/<image_name>')
@basic_auth.required
def get_image(image_name):
    return send_from_directory('/home/nicoz/Desktop/flask_streamcam/flask_stream_stable/frames', image_name)


@app.route('/logout', methods=['POST'])
@basic_auth.required
def logout():
    return "Hai appena effettuto il Log OUT!"

if __name__ == '__main__':
    if not os.path.exists('frames'):
        os.makedirs('frames')
    if not os.path.exists('videos'):
        os.makedirs('videos')
    if not os.path.exists('snapshots'):
        os.makedirs('snapshots')
    try:
        app.run()

    except ssl as e:
        print("Si è verificato un errore SSL: ", e)
        pass
